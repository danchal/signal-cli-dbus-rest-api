#!/bin/sh

set -e

cat > ~/.pypirc <<EOF
[pypi]
  username = __token__
  password = ${PYPI_TOKEN}
EOF

apk --no-cache --quiet --no-progress add \
  py3-pip \
  py3-twine \
pip3 install --upgrade build twine
python3 -m build
twine upload dist/*
