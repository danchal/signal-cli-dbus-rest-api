#!/bin/bash

set -e

if [ ! -z "${DEBUG}" ]; then
  set -x
fi

image="${CI_REGISTRY}/${CI_PROJECT_NAMESPACE,,}/${CI_PROJECT_NAME}/${IMAGE_VARIANT}"

docker login -u gitlab-ci-token -p "${CI_BUILD_TOKEN}" "${CI_REGISTRY}"
apk --no-cache add curl
curl -Lo /usr/local/bin/buildx "https://github.com/docker/buildx/releases/download/v${BUILDX_VERSION}/buildx-v${BUILDX_VERSION}.linux-amd64"
chmod +x /usr/local/bin/buildx
buildx create --name multiarch
buildx use multiarch
buildx inspect --bootstrap
buildx="buildx build --push"
tags="-t ${image}:${CI_COMMIT_REF_SLUG}"
if [[ -n $CI_COMMIT_TAG ]] || [ $CI_COMMIT_BRANCH == "main" ]; then
  tags+=" -t ${image}:latest"
fi
docker pull "${image}":latest || /bin/true
${buildx} --platform ${PLATFORMS} \
  --cache-from "${image}":latest \
  ${tags} \
  -f .docker/Dockerfile .
