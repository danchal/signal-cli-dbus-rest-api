+++
title = "Standalone"
+++

Standalone installation might be useful for environments where Docker is not suitable.

## Install signal-cli

Either install *signal-cli* w/ DBus using [packaging/signal-cli](https://packaging.gitlab.io/signal-cli) or from upstream [signal-cli Releases](https://github.com/AsamK/signal-cli).

{{< tabs groupId="installation" >}}
{{% tab name="Install from packages" %}}
See [Documentation](https://packaging.gitlab.io/signal-cli).
{{% /tab %}}
{{% tab name="Install from signal-cli Releases" %}}
* create a dedicated user: `useradd -r -s /usr/sbin/nologin -m -d /var/lib/signal-cli signal-cli`
* follow [signal-cli installation instructions](https://github.com/AsamK/signal-cli#installation)
  * for all actions using the cli, please run as signal-cli user (`sudo -u signal-cli -H signal-cli ...`)
* setup the [system dbus service](https://github.com/AsamK/signal-cli/wiki/DBus-service#system-bus)
  * make sure `ExecStart` in `signal-cli.service` looks like this:
    * `ExecStart=%your-installation-dir%/bin/signal-cli -a %your_phonenumber% daemon --system`
{{% /tab %}}
{{< /tabs >}}


## Install signal-cli-dbus-rest-api

When *signal-cli* has been installed and setup properly to send messages via DBus, you can install the gateway:

### System packages

* `apt-get install python3-virtualenv python3-pydbus python3-dbus python3-magic`

### Create virtualenv

Virtualenv needs access to system site-packages (DBus bindings).

* `sudo -u signal-cli -i bash -c 'python3 -m virtualenv -p /usr/bin/python3 "${HOME}"/venv --system-site-packages'`

### Install signal-cli-dbus-rest-api

* `sudo -u signal-cli -i bash -c '"${HOME}"/venv/bin/pip install git+https://gitlab.com/morph027/signal-cli-dbus-rest-api@main'`

### Install SystemD units

{{< tabs groupId="systemd" >}}
{{% tab name="signal-cli daemon w/o USERNAME parameter" %}}
* copy [contrib/systemd/signal-cli-dbus-rest-api@.service](https://gitlab.com/morph027/signal-cli-dbus-rest-api/-/raw/main/contrib/systemd/signal-cli-dbus-rest-api@.service) into `/etc/systemd/system/`
* `systemctl enable --now "signal-cli-dbus-rest-api.service@$(systemd-escape <your_phonenumber>)"`
{{% /tab %}}
{{% tab name="signal-cli daemon w/ USERNAME parameter" %}}
* copy [contrib/systemd/signal-cli-dbus-rest-api.service](https://gitlab.com/morph027/signal-cli-dbus-rest-api/-/raw/main/contrib/systemd/signal-cli-dbus-rest-api.service) into `/etc/systemd/system/`
* `systemctl enable --now signal-cli-dbus-rest-api.service`
{{% /tab %}}
{{< /tabs >}}

## Configure signal-cli-dbus-rest-api

Configuration can be done in `/etc/default/signal-cli-dbus-rest-api`. For possible values, see [Docker]({{< ref "installation/docker/#configuration" >}}).

## Install reverse proxy

It is advised to setup a reverse proxy in front of the gateway to add TLS, authentication and so on.
As there are plenty of proxies or webservers: nginx, apache, caddy, traefik,...), just pick your favourite.
