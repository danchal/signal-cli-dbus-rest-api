+++
title = "Docker"
+++


## Run container

{{< tabs groupId="run" >}}
{{% tab name="Docker" %}}
```bash
docker run -d \
  --name signal-cli-dbus-rest-api \
  --env SIGNAL_CLI_DBUS_REST_API_HOST=0.0.0.0 \
  --publish 8080:8080 \
  --volume /some/local/dir/signal-cli-config:/var/lib/signal-cli \
  registry.gitlab.com/morph027/signal-cli-dbus-rest-api/signal-cli-dbus-rest-api:latest
```
{{% /tab %}}
{{% tab name="Docker Compose" %}}
```yaml
version: "3"
services:
  signal-cli-dbus-rest-api:
    image: registry.gitlab.com/morph027/signal-cli-dbus-rest-api/signal-cli-dbus-rest-api:latest
    environment:
      - SIGNAL_CLI_DBUS_REST_API_HOST=0.0.0.0
    ports:
      - "8080:8080"
    volumes:
      - "/some/local/dir/signal-cli-config:/var/lib/signal-cli"
```
{{% /tab %}}
{{< /tabs >}}

## Configuration

{{< tabs groupId="configuration" >}}
{{% tab name="signal-cli-dbus-rest-api" %}}
Variable | Default | Description
---|---|---|
`SIGNAL_CLI_DBUS_REST_API_HOST` | `127.0.0.1` | Address to host the server on.
`SIGNAL_CLI_DBUS_REST_API_PORT` | `8080` | Port to host the server on.
`SIGNAL_CLI_DBUS_REST_API_DEBUG` | `False` | Enables debug output (slows server).
`SIGNAL_CLI_DBUS_REST_API_WORKERS` | `1` | Number of worker processes to spawn.
`SIGNAL_CLI_DBUS_REST_API_ACCESS_LOG` | `False` | Enables log on handling requests (significantly slows server).
{{% /tab %}}
{{% tab name="signal-cli" %}}
Variable | Default | Description
---|---|---|
`SIGNAL_CLI_PARAMS` | `--config /var/lib/signal-cli` | Parameter for `signal-cli` command
`SIGNAL_CLI_DAEMON_PARAMS` | `--system --no-receive-stdout` | Parameters for `signal-cli daemon` subcommand
{{% /tab %}}
{{% tab name="Swagger" %}}

[sanic-openapi configuration](https://sanic-openapi.readthedocs.io/en/stable/sanic_openapi2/configurations.html)

Variable | Default
---|---
`SIGNAL_CLI_DBUS_REST_API_API_HOST` | `127.0.0.1`
`SIGNAL_CLI_DBUS_REST_API_API_PORT` | `8080`
`SIGNAL_CLI_DBUS_REST_API_API_BASEPATH` | `/`
`SIGNAL_CLI_DBUS_REST_API_API_SCHEMES` | `http,https`
{{% /tab %}}
{{< /tabs >}}
