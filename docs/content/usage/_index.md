+++
title = "Usage"
weight = 1
+++

These are some examples. For more info, check the [API documentation](https://morph027.gitlab.io/signal-cli-dbus-rest-api/redoc/).

### Registration

[How to get CAPTCHA!](https://github.com/AsamK/signal-cli/wiki/Registration-with-captcha)

```
curl -d '{"use_voice": true, "captcha": "'${CAPTCHA}'"}' http://localhost:8080/v1/register/${account}
curl -X POST http://localhost:8080/v1/register/${account}/verify/${token}
```

### Plain text

```
curl -d '{"message": "foo"}' http://localhost:8080/v1/send/${phonenumber}
curl -d '{"recipients": ["${phonenumber}"], "message": "foo", "number": "${account}"}' http://localhost:8080/v2/send
```

### With attachments

Attachments needs to be bas64 encoded (e.g. `base64 -w 0 < file`).

```
curl -d '{"message": "bar with image", "base64_attachments": ["base64-encoded-string"]}' http://localhost:8080/v1/send/${phonenumber}
curl -d '{"recipients": ["${phonenumber}"], "message": "bar with image", "number": "${account}", "base64_attachments": ["base64-encoded-string"]}' http://localhost:8080/v2/send
```

### Send to groups

You can find the group id in signal-cli data directory (`~/.local/share/signal-cli/data/<account-id>`)

{{% notice info %}}
**As the group id might contain slashes, which will break the request, you need to replace `/` (slash) with `-` (dash) for v1**
{{% /notice %}}

```
curl -d '{"message": "foobar to all"}' http://localhost:8080/v1/send/${groupid.replace("/", "-")]
curl -d '{"recipients": ["${groupid}"], "message": "foobar to all", "number": "${account}"}' http://localhost:8080/v2/send
```
